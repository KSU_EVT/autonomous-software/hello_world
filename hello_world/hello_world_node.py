from dataclasses import dataclass
from typing import List

@dataclass
class Member:
    name: str
    message: str

def main():
    members: List[Member] = [
        Member('Sahan', 'I live in Wilder (help)'),
    ]

    for member in members:
        print(f'{member.name} - {member.message}')
    print('Welcome to EVT!')


if __name__ == '__main__':
    main()

